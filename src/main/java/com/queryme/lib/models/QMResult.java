package com.queryme.lib.models;

import java.util.ArrayList;
import java.util.List;

public class QMResult{

	public String message;
	public String[] columns;
	public List<String[]> rows;
	public Object rawData;
	{
		columns = new String[] {};
		rows = new ArrayList<String[]>();
	}

	public int getSize() {
		return rows.size();
	}
}
