package com.queryme.lib.models;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Created by rramirezb on 12/01/2015.
 */
public class SocketStreams implements Streams<BufferedReader, PrintWriter> {
    private Socket socket;
    private final String host;
    private final int port;
    private final int timeout;
    private PrintWriter pw;
    private BufferedReader br;

    public SocketStreams(String host, int port, int timeout) {
        this.host = host;
        this.port = port;
        this.timeout = timeout;
    }

    @Override
    public void connect() {
        socket = new Socket();

        try {
            socket.connect(new InetSocketAddress(host, port), timeout);
            // System.out.println("Obteniendo printWriter");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void disconnect() {

        try {
            pw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public BufferedReader getInput() {
        try {
            br = new BufferedReader(new InputStreamReader(
                    socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return br;
    }

    @Override
    public PrintWriter getOutput() {

        try {
            pw = new PrintWriter(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pw;
    }
}
