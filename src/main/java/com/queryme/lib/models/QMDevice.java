package com.queryme.lib.models;

public class QMDevice {
//	public String packageName;
	public String deviceId;
	// Build.DEVICE;
	public String device;
	// Build.USER;
	public String user;
	// Build.PRODUCT;
	public String product;
	// Build.MANUFACTURER;
	public String manufacter;
	// Build.BRAND;
	public String brand;
	// Build.MODEL
	public String model;
	// Build.ID;
	public String id;
	// Build.HARDWARE;
	public String hardware;
//	public String packageVersion;
}
