package com.queryme.lib.models;


import java.io.BufferedReader;
import java.io.PrintWriter;

/**
 * Created by rramirezb on 12/01/2015.
 */
public interface Streams<I extends BufferedReader, O extends PrintWriter> {
    public void connect();
    public void disconnect();
    public I getInput();
    public O getOutput();
}
