package com.queryme.lib.models;

/**
 * Created by rramirezb on 12/01/2015.
 */
public class ConnInfo {
    private final String host;
    private final int port;
    private final int timeout;

    public ConnInfo(String host, int port, int timeout) {
        this.host = host;
        this.port = port;
        this.timeout = timeout;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public int getTimeout() {
        return timeout;
    }
}
