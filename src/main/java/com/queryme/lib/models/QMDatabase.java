package com.queryme.lib.models;

/**
 * Created by rramirezb on 09/01/2015.
 */
public class QMDatabase {
    public String appPackage;
    public String name;
    public int version;

    public QMDatabase() {
    }

    public QMDatabase(String appPackage, String name, int version) {
        this.appPackage = appPackage;
        this.name = name;
        this.version = version;
    }
}
