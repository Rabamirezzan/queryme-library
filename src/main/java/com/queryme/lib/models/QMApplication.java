package com.queryme.lib.models;

/**
 * Created by rramirezb on 09/01/2015.
 */
public class QMApplication{
    public String name;
    public String packageName;
    public String versionName;
    public int version;

    public QMApplication() {
    }

    public QMApplication(String name, String packageName, String versionName, int version) {
        this.name = name;
        this.packageName = packageName;
        this.versionName = versionName;
        this.version = version;
    }
}
