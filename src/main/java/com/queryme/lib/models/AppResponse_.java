package com.queryme.lib.models;

/**
 * Created by rramirezb on 12/01/2015.
 */
public class AppResponse_<D> implements AppResponse<D> {
    private D data;
    private String message;

    public AppResponse_() {
        message = "No message";
    }

    public AppResponse_(D data, String message) {
        this.data = data;
        this.message = message;
    }

    @Override
    public D data() {
        return data;
    }

    @Override
    public String message() {
        return message;
    }
}
