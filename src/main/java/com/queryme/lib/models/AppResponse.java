package com.queryme.lib.models;

/**
 * Created by rramirezb on 09/01/2015.
 */
public interface AppResponse<D> {
    public D data();
    public String message();

}
