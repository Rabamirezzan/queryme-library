package com.queryme.lib.models;

public class Info {
	public int dbVersion;
	public String dbName;
	// String deviceName = "";
	public String appPackage = "";
	public String nickname = "";
	public String ip = "";
	public int port = 0;
	public int timeout = 1;
}
