package com.queryme.lib.datainput;

import com.queryme.lib.tools.ProcessID;
import com.queryme.lib.validators.IValidator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.atomic.AtomicBoolean;



public class UserInput {
	BufferedReader br;

	public UserInput() {
		br = new BufferedReader(new InputStreamReader(System.in));
	}

	public String getData(String inputInformation) {
		System.out.println(inputInformation);
		String line = "";
		try {
			line = br.readLine();
		} catch (IOException e) {

			e.printStackTrace();
		}
		return line;
	}

	public <V extends IValidator<String>> String getData(ProcessID process,
			String inputInformation, int attempts, AtomicBoolean success, V... validator) {
		int attempt = 0;
		String data = null;
		boolean result = false;
		if(success==null){
			success = new AtomicBoolean();
			
		}
		do {
			data = getData(inputInformation);
			result = IValidator.Ops.validate(process, data, validator); 
			if (result) {
				
				attempt = attempts;
			} else {
				attempt++;
			}
		} while (attempt < attempts);

		success.set(result);
		return data;
	}
}
