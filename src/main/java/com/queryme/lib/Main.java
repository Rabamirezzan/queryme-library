package com.queryme.lib;

import java.util.List;
import java.util.Map;

public class Main {

	private static void main1(String... args) {
        System.out.println("Query Me");
        System.out.println("author rramirez.");
        System.out.println("");
        Map<String, List<String>> commands = CommanderTool.getParams(args);
		QueryMeExec.main(commands);
	}

	public static void main(String[] args) {

		main1(args);
	}
}
