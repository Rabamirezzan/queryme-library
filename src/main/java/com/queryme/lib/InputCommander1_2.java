package com.queryme.lib;


import com.queryme.lib.datainput.UserInput;
import com.queryme.lib.models.Info;
import com.queryme.lib.process.QueryMeProcess;
import com.queryme.lib.tools.ChainSequence;
import com.queryme.lib.tools.ChainSequence.Link;
import com.queryme.lib.tools.NicknameTool;
import com.queryme.lib.validators.IValidationListener;
import com.queryme.lib.validators.impl.ValidAllListener;
import com.queryme.lib.validators.impl.ValidIP;
import com.queryme.lib.validators.impl.ValidPort;

import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.queryme.lib.CommanderTool.*;

public class InputCommander1_2 implements CommanderTool.ICommander {

	private static final String[] REQUIRED_PARAMS = { IP_P, PORT_P};//, APP_P,
			//DB_NAME_P, DB_VERSION_P };

	Info info;

	public InputCommander1_2(Info info) {
		super();
		this.info = info;
	}

	@Override
	public String[] getRequiredParams() {
		// TODO Auto-generated method stub
		return REQUIRED_PARAMS;
	}

	@Override
	public boolean onNickname(String... args) {
		boolean result = args.length > 0;
		if (result) {
			info.nickname = NicknameTool.getNickname(args);
		}
		return result;
	}

	@Override
	public boolean onIP(String... args) {
		boolean result = args.length > 0;
		if (result) {
			info.ip = args[0];
		}
		return result;
	}

	@Override
	public boolean onPort(String... args) {
		boolean result = args.length > 0;
		if (result) {
			try {
				info.port = Integer.valueOf(args[0]);
			} catch (NumberFormatException e) {
				result = false;

			}
		}
		return result;
	}

	@Override
	public boolean onApp(String... args) {
		boolean result = args.length > 0;
		if (result) {
			info.appPackage = args[0];
		}
		return result;
	}

	@Override
	public boolean onDBName(String... args) {
		boolean result = args.length > 0;
		if (result) {
			info.dbName = args[0];
		}
		return result;
	}

	@Override
	public boolean onDBVersion(String... args) {
		boolean result = args.length > 0;
		if (result) {
			try {
				info.dbVersion = Integer.valueOf(args[0]);
			} catch (NumberFormatException e) {
				result = false;

			}
		}
		return result;
	}

	@Override
	public boolean onHelp(String... args) {
		CommanderTool.showHelp();
		return true;
	}

	@Override
	public boolean onWizard(String... args) {
		boolean result = false;
		final AtomicBoolean success = new AtomicBoolean(true);
		IValidationListener.Ops.register(ValidAllListener.class);
		final Map<String, String> wizard = CommanderTool.getWizard();
		final UserInput input = new UserInput();

		ChainSequence chain = new ChainSequence();
		chain.add(new ChainSequence.Link() {
			@Override
			protected boolean proccess() {
				String value = wizard.get(CommanderTool.IP_P);
				value = input.getData(QueryMeProcess.INPUT_DATA, value, 2,
						success, new ValidIP());
				info.ip = value;
				return success.get();
			}
		});

		chain.add(new Link() {
			@Override
			protected boolean proccess() {
				String value = wizard.get(CommanderTool.PORT_P);
				value = input.getData(QueryMeProcess.INPUT_DATA, value, 2,
						success, new ValidPort());
				info.port = Integer.valueOf(value);
				return success.get();
			}
		});

//		chain.add(new Link() {
//			@Override
//			protected boolean proccess() {
//				String value = wizard.get(CommanderTool.NICKNAME_P);
//				value = input.getData(value);
//				info.nickname = value;
//				return success.get();
//			}
//		});

//		chain.add(new Link() {
//			@Override
//			protected boolean proccess() {
//				String value = wizard.get(CommanderTool.APP_P);
//				value = input.getData(QueryMeProcess.INPUT_DATA, value, 2,
//						success, new ValidAppInformation());
//				info.appPackage = value;
//				return success.get();
//			}
//		});

//		chain.add(new Link() {
//			@Override
//			protected boolean proccess() {
//				String value = wizard.get(CommanderTool.DB_NAME_P);
//				value = input.getData(QueryMeProcess.INPUT_DATA, value, 2,
//						success, new ValidAppInformation());
//				info.dbName = value;
//				return success.get();
//			}
//		});

//		chain.add(new Link() {
//			@Override
//			protected boolean proccess() {
//				String value = wizard.get(CommanderTool.DB_VERSION_P);
//				value = input.getData(QueryMeProcess.INPUT_DATA, value, 2,
//						success, new ValidAppInformation());
//				info.dbVersion = Integer.valueOf(value);
//				onFinish();
//				return success.get();
//			}
//		});

		result = chain.execute();
		return result;
	}

	@Override
	public boolean onFinish(String... args) {
		CommanderTool.setNicknameOnFinishCommander(info);
		return false;
	}

}
