package com.queryme.lib;


import com.queryme.lib.validators.IValidator;

public class HelpConstructor<T> {
	private Class<IValidator<T>> validator;
	private Object[] args;

	public HelpConstructor(Class<IValidator<T>> validator, Object... args) {
		super();
		this.validator = validator;
		this.args = args;
	}

	public Class<IValidator<T>> getValidator() {
		return validator;
	}

	public Object[] getArgs() {
		return args;
	}
}
