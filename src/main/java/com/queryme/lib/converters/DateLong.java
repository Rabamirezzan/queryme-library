package com.queryme.lib.converters;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateLong extends QConverter<String> {

	SimpleDateFormat sdf;
	{
		sdf = new SimpleDateFormat("yyyy.MM.dd HH.mm.ss.SSS");
	}

	public DateLong(String[] fields) {
		super(fields);

	}

	@Override
	public String convertData(Object id, Serializable data) {
		String result = String.valueOf(data);
		if (data != null && !result.toLowerCase().equals("null")) {
			Date date = new Date(Long.valueOf(((String) data)));
			result = sdf.format(date);
		}
		return result;
	}

}
