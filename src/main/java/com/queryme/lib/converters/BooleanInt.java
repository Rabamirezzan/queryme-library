package com.queryme.lib.converters;

import java.io.Serializable;

public class BooleanInt extends QConverter<String> {

	
	public BooleanInt() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BooleanInt(String[] fields) {
		super(fields);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String convertData(Object id, Serializable data) {
		boolean parsedValue = false;
		String result = String.valueOf(data);
		try {
			int value = Integer.valueOf((String) data);
			parsedValue = value == 0 ? false : true;
			result = String.valueOf(parsedValue);
		} catch (NumberFormatException e) {

		}

		return result;
	}

}
