package com.queryme.lib.converters;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

public abstract class QConverter<T extends Serializable> {
	//public static Map<Object, QConverter<? extends Serializable>> convertersByField;
    public static Map<Class<? extends QConverter<?>>, QConverter<? extends Serializable>> converters;
	protected Set<String> fields;
	static {
		//convertersByField = new TreeMap<Object, QConverter<? extends Serializable>>();
        converters = new HashMap<Class<? extends QConverter<?>>, QConverter<? extends Serializable>>();
	}

    {
        fields = new TreeSet<String>();
    }

	public QConverter(String[] fields) {
		this.fields.addAll(Arrays.asList(fields));
	}

	private static final Object[] data(String[] data) {
		Object[] ob = new Object[data.length];
		for (int i = 0; i < data.length; i++) {
			String string = data[i];
			ob[i] = string;
		}

		return ob;

	}

	@SuppressWarnings("unchecked")
	public static void addConverter(
			Class<? extends QConverter<? extends Serializable>> converterClass,
			String[] fields) {

		try {
			Constructor<?> constructor = converterClass
					.getDeclaredConstructor(String[].class);

			Object param = fields;
            if(!converters.containsKey(converterClass)){
                QConverter<? extends Serializable> instance = (QConverter<? extends Serializable>) constructor
                        .newInstance(param);
                converters.put(converterClass, instance);
            }else{
                converters.get(converterClass).addFields(fields);
            }


		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// public static QConverter<? extends Serializable> getConverter(Object
	// id) {
	// QConverter<? extends Serializable> instance = convertersByField.get(id);
	//
	// return instance;
	// }

    public static QConverter<? extends Serializable>  getConverterByField(Object id){
        Iterator<QConverter<? extends Serializable>> it = converters.values().iterator();

        QConverter<? extends Serializable> result=null;
        while (it.hasNext()) {
            QConverter<? extends Serializable> next = it.next();
            if(next.containsField((String)id)){
                result = next;
                break;
            }
        }
        return result;
    }

    private boolean containsField(String id) {
        return fields.contains(id);
    }

    private static final QConverter<? extends Serializable> DEFAULT_CONVERTER =  new QConverter<Serializable>() {

        @Override
        public Object[] getFields() {

            return null;
        }

        @Override
        public Serializable convertData(Object id, Serializable data) {

            return data;
        }
    };
    @SuppressWarnings("unchecked")
	public static <T extends Serializable> T convert(Object id, T data) {
		QConverter<? extends Serializable> instance = getConverterByField(id);
		if (instance == null) {
			instance = DEFAULT_CONVERTER;

		}
		return (T) instance.convertData(id, data);
	}

	public QConverter() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Object[] getFields() {
		return fields.toArray(new String[]{});
	}

    public void addFields(String... fields){
        this.fields.addAll(Arrays.asList(fields));
    }

	public abstract T convertData(Object id, Serializable data);

}
