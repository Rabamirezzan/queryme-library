package com.queryme.lib.converters;

import com.queryme.lib.Deserializer;

import java.io.Serializable;
import java.util.ArrayList;



public class ArrayListC extends QConverter<ArrayList<Integer>> {

	public ArrayListC(String[] fields) {
		super(fields);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ArrayList<Integer> convertData(Object id, Serializable data) {
		// TODO Auto-generated method stub
		// System.out.println(id + " " + data);

		ArrayList<Integer> list = Deserializer.toObject((String) data);
		return list;
	}

}
