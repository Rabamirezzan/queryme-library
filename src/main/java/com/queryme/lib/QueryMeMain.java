package com.queryme.lib;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.io.Serializable;
//import java.text.DecimalFormat;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Map;
//import java.util.TreeMap;
//
//import com.example.sqlremoteconnector.QueryMe;
//import com.opesystems.sqlremote.QMDevice;
//import com.opesystems.sqlremote.QMResult;
//
///**
// * @author rramirezb
// */
public class QueryMeMain {

}
//	public static boolean startsWith(String sentence, String... data) {
//		boolean result = false;
//		for (String d : data) {
//			result |= d.startsWith(d);
//		}
//
//		return result;
//	}
//
//	public static void main(String[] args) {
//		QueryMe queryMe = new QueryMe();
//		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//
//		QConverter.addConverter(ArrayListC.class);
//		String deviceName = "";
//		String app = "";
//		String nickname = "";
//		String ip = "";
//		int port = 0;
//		int timeout = 1;
//		String appPackage = "";
//		String dbName = "";
//		int dbVersion = 0;
//		boolean connected = false;
//		do {
//			try {
//
//				ip = "";
//				port = 0;
//				try {
//					System.out.println("Label?");
//					nickname = br.readLine();
//					System.out.println("Host?");
//					ip = br.readLine();
//					System.out.println("Port? ie. 9221");
//					port = Integer.valueOf(br.readLine());
//					System.out.println("App?");
//					appPackage = br.readLine();
////					System.out.println("dbName?");
////					dbName = br.readLine();
////					System.out.println("dbVersion");
////					dbVersion = Integer.valueOf(br.readLine());
//				} catch (NumberFormatException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				} catch (IOException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				}
//
//				timeout = 10000;
//				QMDevice device = queryMe.getDeviceInfo(ip, port, timeout);
//				deviceName = device.product;
//				app = device.packageName;
//				System.out.println("Device: " + device.product + " "
//						+ device.model + " " + device.manufacter);
//				System.out.println("App: " + device.packageName);
//				connected = true;
//			} catch (Exception e1) {
//				// TODO Auto-generated catch block
//				System.out.println("err " + e1);
//				// e1.printStackTrace();
//			}
//		} while (!connected);
//		// device = queryMe.getDeviceInfo(ip, port, timeout);
//		// System.out.println("Device: " + device.product + " " + device.model
//		// + " " + device.manufacter);
//		// System.out.println("App: " + device.packageName);
//		String line = nickname + "/" + deviceName + "/" + app + " SQL>";
//		do {
//			try {
//
//				System.out.println(line);
//				String params = br.readLine();
//
//				try {
//					QMResult result = null;
//					if (startsWith(params.trim().toLowerCase(), "select",
//							"pragma")) {
//						result = queryMe.execCommand(ip, port, timeout,
//								QMResult.class, appPackage, dbName, dbVersion,
//								QueryMe.QUERY_COMMAND, params);
//					} else {
//						result = queryMe.execCommand(ip, port, timeout,
//								QMResult.class, appPackage, dbName, dbVersion,
//								QueryMe.EXEC_COMMAND, params);
//					}
//
//					System.out.println("Msg: " + result.message);
//					System.out.println(Arrays.asList(result.columns));
//					DecimalFormat df = new DecimalFormat("0000");
//					for (int i = 0; i < result.rows.size(); i++) {
//						System.out.print(df.format(i + 1) + " - ");
//						for (int j = 0; j < result.columns.length; j++) {
//							String column = result.columns[j];
//							Serializable data = result.rows.get(i)[j];
//							// Class<? extends Converter<?>> cClass =
//							data = QConverter.convert(column, data);
//
//							System.out.print(data);
//
//							if (j < result.columns.length - 1) {
//								System.out.print(", ");
//							}
//							// if(cClass==null){
//							// data = result.rows.get(i)[j];
//							// }else{
//							// data
//							// }
//						}
//						System.out.println();
//						// System.out.print(df.format(i + 1) + " - "
//						// + Arrays.asList(result.rows.get(i)));
//					}
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		} while (true);
//
//		// queryMe.stop();
//	}
//
//	public static class ArrayListC extends QConverter<ArrayList<Integer>> {
//
//		@Override
//		public Object[] getFields() {
//			// TODO Auto-generated method stub
//			return new Object[] { "days", "indexes", "statusList" };
//		}
//
//		@Override
//		public ArrayList<Integer> convertData(Object id, Serializable data) {
//			// TODO Auto-generated method stub
//			// System.out.println(id + " " + data);
//
//			ArrayList<Integer> list = Deserializer.toObject((String) data);
//			return list;
//		}
//
//	}
//
//	public static abstract class QConverter<T extends Serializable> {
//		public static Map<Object, QConverter<? extends Serializable>> convertersByField;
//		static {
//			convertersByField = new TreeMap<Object, QConverter<? extends Serializable>>();
//		}
//
//		public static void addConverter(
//
//		Class<? extends QConverter<? extends Serializable>> converterClass) {
//			try {
//				QConverter<? extends Serializable> instance = converterClass
//						.newInstance();
//				Object[] it = instance.getFields();
//				for (Object field : it) {
//					convertersByField.put(field, instance);
//				}
//
//			} catch (InstantiationException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IllegalAccessException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//
//		// public static QConverter<? extends Serializable> getConverter(Object
//		// id) {
//		// QConverter<? extends Serializable> instance = convertersByField.get(id);
//		//
//		// return instance;
//		// }
//
//		public static <T extends Serializable> T convert(Object id, T data) {
//			QConverter<? extends Serializable> instance = convertersByField.get(id);
//			if (instance == null) {
//				instance = new QConverter<Serializable>() {
//
//					@Override
//					public Object[] getFields() {
//						// TODO Auto-generated method stub
//						return null;
//					}
//
//					@Override
//					public Serializable convertData(Object id, Serializable data) {
//						// TODO Auto-generated method stub
//						return data;
//					}
//				};
//				convertersByField.put(id, instance);
//			}
//			return (T) instance.convertData(id, data);
//		}
//
//		public QConverter() {
//			super();
//			// TODO Auto-generated constructor stub
//		}
//
//		public abstract Object[] getFields();
//
//		public abstract T convertData(Object id, Serializable data);
//	}
//}
