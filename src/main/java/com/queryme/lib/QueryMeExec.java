package com.queryme.lib;

import com.queryme.lib.converters.ArrayListC;
import com.queryme.lib.converters.BooleanInt;
import com.queryme.lib.converters.DateLong;
import com.queryme.lib.converters.QConverter;
import com.queryme.lib.datainput.UserInput;
import com.queryme.lib.models.*;
import com.queryme.lib.process.*;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


public class QueryMeExec {


    private static String getDeviceInfo(QueryMeCalls queryMe,
                                        String ip, int port, int timeout) {
        QMDevice device = queryMe.getDeviceInfo();
        String deviceName = device.product;

        System.out.println("Device: " + device.product + " " + device.model
                + " " + device.manufacter);


        return deviceName.concat("/");
    }

    private static void setDBInfo(QueryMeCalls queryMe, Info info) {
        // if (!info.dbName.trim().equals("") &&
        // !info.dbVersion.trim().equals("")) {
        queryMe.setDatabaseInfo(
                info.appPackage, info.dbName, info.dbVersion);
        // }

    }

    private static CommanderTool.ICommander getCommander(String version, Info info) {
        CommanderTool.ICommander commander = null;
        if (version.equals("1.2")) {
            commander = new InputCommander1_2(info);
        } else if (version.equals("1.1")) {
            commander = new InputCommander1_1(info);
        }
        return commander;
    }

    private static boolean isQuit(String params) {

        return Utils.startsWith(params.toLowerCase(), "q") && params.length() == 1;
    }







    private static AppResponse<?> execute(String params, Info info, QueryMeCalls queryMe) {
        AppResponse<?> result = null;
        if (CommandUtilities.isCommand(params, QueryMeCommands.GET_APP_LIST_COMMAND)) {
            result = queryMe.getApplicationsList();

        } else if (CommandUtilities.isCommand(params, QueryMeCommands.SELECT_APP_COMMAND)) {
            queryMe.appIndex = CommandUtilities.getNumber(params);

            result = queryMe.getDatabases(queryMe.appIndex);

        } else if (CommandUtilities.isCommand(params, QueryMeCommands.SELECT_DATABASE_COMMAND)) {
            int appIndex = queryMe.appIndex;
            int dbIndex = CommandUtilities.getNumber(params);

            result = queryMe.selectDatabase(appIndex, dbIndex);

        } else if (CommandUtilities.isSQLQueryCommand(params)) {
            result = queryMe.executeQuery( params);
        } else {
            result = queryMe.execute( params);
        }
        return result;
    }

    private static AppResponse<?> sharedPreferences(String params, Info info,
                                              QueryMe queryMe) {
        String ps = params.trim();
        if (ps.length() > 2) {
            ps = ps.substring(2, ps.length()).trim();
        } else {
            ps = "{}";
        }

        AppResponse<?> result = queryMe.execCommand(

                DataTypes.getResponseQMResultType(),
                QueryMeCommands.SHARED_PREFERENCES_COMMAND, ps);
        return result;
    }

    private static void printHeaderResult(QMResult result) {
        String header = Arrays.asList(result.columns).toString();// .replace(",",
        // "\t");
        System.out.println(String.format("columns: %s", header));
    }

    private static void printFooterResult(AppResponse<?> response) {
        System.out.println("Msg: " + response.message());
    }

    private static int getMax(Map<Integer, Integer> maxSize, int index,
                              int value) {
        int actualValue = maxSize.get(index);
        actualValue = Math.max(actualValue, value);
        maxSize.put(index, actualValue);
        return actualValue;
    }

    // private static String getTabs(int actualSize, int maxSize){
    // int size = (int)((maxSize-actualSize)/4.0);
    // StringBuilder builder = new StringBuilder();
    // for (int i = 0; i < size; i++) {
    //
    // }
    // }

    private static void printBodyResult(QMResult result) {
        printBodyResultJsonRows(result);
    }

    private static void printBodyResultJsonRows(QMResult result) {
        DecimalFormat df = new DecimalFormat("0000");
        // Map<Integer, Integer> minSize = new HashMap<Integer, Integer>();
        String pattern = " %s {\n %s \n }";

        for (int i = 0; i < result.rows.size(); i++) {
            StringBuilder builder = new StringBuilder();
            for (int j = 0; j < result.columns.length; j++) {
                String sdata = "";
                String column = result.columns[j];
                Serializable data = result.rows.get(i)[j];
                try {

                    // Class<? extends Converter<?>> cClass =
                    data = QConverter.convert(column, data);
                    sdata = data != null ? data.toString() : "[NULL]";
                } catch (Exception e) {
                    sdata = String.format("Column %s: ex: %s", column,
                            e.getMessage());
                    // e.printStackTrace();
                }
                builder.append(column);
                builder.append(":");
                builder.append(sdata);


                // int max = getMax(maxSize, i, sdata.length());
                if (j < result.columns.length - 1) {
                    builder.append(",\n ");
                }

            }

            System.out.println(String.format(pattern, df.format(i + 1),
                    builder.toString()));

        }
    }

    private static void printBodyResultTable(QMResult result) {
        DecimalFormat df = new DecimalFormat("0000");
        // Map<Integer, Integer> minSize = new HashMap<Integer, Integer>();
        for (int i = 0; i < result.rows.size(); i++) {
            System.out.print(df.format(i + 1) + " - ");
            for (int j = 0; j < result.columns.length; j++) {
                String sdata = "";
                String column = result.columns[j];
                Serializable data = result.rows.get(i)[j];
                try {

                    // Class<? extends Converter<?>> cClass =
                    data = QConverter.convert(column, data);
                    sdata = data != null ? data.toString() : "[NULL]";
                } catch (Exception e) {
                    sdata = String.format("Column %s: ex: %s", column,
                            e.getMessage());
                    // e.printStackTrace();
                }

                System.out.print(sdata);
                // int max = getMax(maxSize, i, sdata.length());
                if (j < result.columns.length - 1) {
                    System.out.print(",    ");
                }

            }
            System.out.println();

        }
    }

    private static String getCommandLine(StringBuilder builder, Info info, QueryMeCalls queryMe) {
        String line = "";
        try {
            line = String.format("%s - %s - %s [%s v %s]>", info.nickname,

                    getDeviceInfo(queryMe, info.ip,
                            info.port, info.timeout), info.appPackage, info.dbName, info.dbVersion);
        } catch (Exception e) {
            System.out.println("Unabled to get device info: " + e.getMessage());
             e.printStackTrace();
        }
        builder.delete(0, builder.length());
        builder.append(line);
        return line;
    }

    public static String[] getArrayListColumns(
            Map<String, List<String>> parameters) {
        List<String> columns = new ArrayList<String>();


        List<String> data = parameters.get(CommanderTool
                .getCommand(CommanderTool.ARRAYLIST_COLUMNS_P));
        if (data != null) {
            columns.addAll(data);
        }
        return columns.toArray(new String[]{});
    }

    public static String[] getDatetimeColumns(
            Map<String, List<String>> parameters) {
        List<String> columns = new ArrayList<String>();
        // "created", "deliveryDate", "checkin", "checkout"


        List<String> data = parameters.get(CommanderTool
                .getCommand(CommanderTool.DATE_COLUMNS_P));
        if (data != null) {
            columns.addAll(data);
        }
        return columns.toArray(new String[]{});
    }

    public static String[] getBooleanColumns(
            Map<String, List<String>> parameters) {
        List<String> columns = new ArrayList<String>();

        List<String> data = parameters.get(CommanderTool
                .getCommand(CommanderTool.BOOLEAN_COLUMNS_P));
        if (data != null) {
            columns.addAll(data);
        }
        return columns.toArray(new String[]{});
    }

    public static void main(Map<String, List<String>> parameters) {
        Info info = new Info();

        QConverter.addConverter(ArrayListC.class,
                getArrayListColumns(parameters));

        QConverter.addConverter(DateLong.class, getDatetimeColumns(parameters));
        QConverter
                .addConverter(BooleanInt.class, getBooleanColumns(parameters));

        UserInput input = new UserInput();

        String version = CommanderTool.getCommanderVersion(parameters, "1.2");

        System.out.println("Version: " + version);

        CommanderTool.ICommander commander = getCommander(version, info);
        boolean connected = false;


        try {

            connected = CommanderTool.executeCommands(parameters, commander);
            info.timeout = 10000;

            QueryMe queryMe = new QueryMe(new SocketStreams(info.ip, info.port, 1000));

            QueryMeCalls calls = new QueryMeCalls(queryMe);

            StringBuilder line = new StringBuilder();
            if (connected) {
                getCommandLine(line, info, calls);

                connected = line.length() > 0;
                System.out.println("QUIT: q/Q");

                while (connected) {

                    try {
                        String params = input.getData(line.toString());
                        AppResponse<?> response;
                        if (isQuit(params)) {
                            break;
                        } else if (isSharedPreference(params)) {
                            //setDBInfo(queryMe, info);
                            response = sharedPreferences(params, info, queryMe);
                            if (response instanceof QMResult) {
                                QMResult result = (QMResult) response;
                                System.out.println(result.rawData);
                            }
                        } else {

                            //setDBInfo(queryMe, info);
                            response = execute(params, info, calls);

                            updateDBInfo(response, info, calls, line);
                            printApps(response);
                            printDatabases(response);
                            printQueryResult(response);

                        }

                            printFooterResult(response);


                    } catch (Exception e) {
                        System.out.println("Unabled to send commands: "
                                + e.getMessage());
                         e.printStackTrace();
                    }

                }
            }
        } catch (Exception e1) {

            System.out.println("Unabled to get commands: " + e1.getMessage());
            e1.printStackTrace();
        }
        exit();

    }

    private static void printQueryResult(AppResponse<?> response) {
        Object data = response.data();
        if (data instanceof QMResult) {
            QMResult result = (QMResult) data;
            printHeaderResult(result);
            printBodyResult(result);
        }

    }

    public static void deleteInput(StringBuilder builder) {
        if (builder != null) {
            builder.delete(0, builder.length());
        }
    }

    private static void updateDBInfo(AppResponse<?> response, Info info, QueryMeCalls queryMe, StringBuilder line) {
        Object data = response.data();
        if (data instanceof QMDatabase) {
            info.appPackage = ((QMDatabase) data).appPackage;
            info.dbName = ((QMDatabase) data).name;
            info.dbVersion = ((QMDatabase) data).version;
            getCommandLine(line, info, queryMe);
        }
    }

    public static void printApps(AppResponse<?> response) {
        Object data = response.data();
        if (data instanceof List) {
            List<?> list = (List<?>) data;
            if (!list.isEmpty()) {
                if (list.get(0) instanceof QMApplication) {
                    List<QMApplication> apps = (List<QMApplication>) list;
                    int i = 1;
                    System.out.printf("Applications\n");
                    for (QMApplication app : apps) {
                        System.out.printf("%s-> %s v. %s [%s] \n", i++, app.packageName, app.versionName, app.version);
                    }
                }
            }
        }

    }

    public static void printDatabases(AppResponse<?> response) {
        Object data = response.data();
        if (data instanceof List) {
            List<?> list = (List<?>) data;
            if (!list.isEmpty()) {
                if (list.get(0) instanceof QMDatabase) {
                    List<QMDatabase> dbs = (List<QMDatabase>) list;
                    int i = 1;
                    for (QMDatabase db : dbs) {
                        System.out.printf("%s-> %s v. %s\n", i++, db.name, db.version);
                    }
                }
            }
        }

    }

    private static boolean isSharedPreference(String line) {
        boolean result = line.trim().startsWith("sp");
        return result;
    }

    private static void exit() {
        System.out.println("EXIT(0)");
        System.exit(0);
    }
}
