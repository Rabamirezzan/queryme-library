package com.queryme.lib;


import com.queryme.lib.models.Info;
import com.queryme.lib.tools.ChainSequence.Link;

import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.queryme.lib.CommanderTool.IP_P;
import static com.queryme.lib.CommanderTool.PORT_P;


public class InputCommander1_1 implements CommanderTool.ICommander {
	private static final String[] REQUIRED_PARAMS = { IP_P, PORT_P };

	Info info;

	public InputCommander1_1(Info info) {
		super();
		this.info = info;
		initInfo();
	}

	private void initInfo() {
		this.info.appPackage = "";
		this.info.dbName = "";
		this.info.dbVersion = 1;
	}

	@Override
	public String[] getRequiredParams() {

		return REQUIRED_PARAMS;
	}

	@Override
	public boolean onNickname(String... args) {
		boolean result = args.length > 0;
		if (result) {
			info.nickname = com.queryme.lib.tools.NicknameTool.getNickname(args);
		}
		return result;
	}

	@Override
	public boolean onIP(String... args) {
		boolean result = args.length > 0;
		if (result) {
			info.ip = args[0];
		}
		return result;
	}

	@Override
	public boolean onPort(String... args) {
		boolean result = args.length > 0;
		if (result) {
			try {
				info.port = Integer.valueOf(args[0]);
			} catch (NumberFormatException e) {
				result = false;

			}
		}
		return result;
	}

	@Override
	public boolean onApp(String... args) {
		boolean result = args.length > 0;
		if (result) {
			info.appPackage = args[0];
		}
		return result;
	}

	@Override
	public boolean onDBName(String... args) {
		boolean result = args.length > 0;
		if (result) {
			info.dbName = args[0];
		}
		return result;
	}

	@Override
	public boolean onDBVersion(String... args) {
		boolean result = args.length > 0;
		if (result) {
			try {
				info.dbVersion = Integer.valueOf(args[0]);
			} catch (NumberFormatException e) {
				result = false;

			}
		}
		return result;
	}

	@Override
	public boolean onHelp(String... args) {
		com.queryme.lib.CommanderTool.showHelp();
		return true;
	}

	@Override
	public boolean onWizard(String... args) {
		final Map<String, String> wizard = com.queryme.lib.CommanderTool.getWizard();
		final com.queryme.lib.datainput.UserInput input = new com.queryme.lib.datainput.UserInput();
		final AtomicBoolean success = new AtomicBoolean(true);

		com.queryme.lib.validators.IValidationListener.Ops.register(com.queryme.lib.validators.impl.ValidAllListener.class);

		com.queryme.lib.tools.ChainSequence chain = new com.queryme.lib.tools.ChainSequence();
		chain.add(new Link() {
			@Override
			protected boolean proccess() {
				String label = wizard.get(com.queryme.lib.CommanderTool.IP_P);
				label = input.getData(com.queryme.lib.process.QueryMeProcess.INPUT_DATA, label, 2, success, new com.queryme.lib.validators.impl.ValidIP());
				info.ip = label;
				return success.get();
			}
		});

		chain.add(new Link() {
			@Override
			protected boolean proccess() {
				String label = wizard.get(com.queryme.lib.CommanderTool.PORT_P);
				label = input.getData(com.queryme.lib.process.QueryMeProcess.INPUT_DATA, label, 2, success, new com.queryme.lib.validators.impl.ValidPort());
				info.port = Integer.valueOf(label);
				return success.get();
			}
		});
		chain.add(new Link() {
			@Override
			protected boolean proccess() {
				String label = wizard.get(com.queryme.lib.CommanderTool.NICKNAME_P);
				label = input.getData(label);
				info.nickname = label;
				if (success.get())
					onFinish();
				return success.get();
			}
		});
		// label = wizard.get(CommanderTool.APP_P);
		// label = input.getData(label);
		// info.appPackage = label;
		//
		// label = wizard.get(CommanderTool.DB_NAME_P);
		// label = input.getData(label);
		// info.dbName = label;
		//
		// label = wizard.get(CommanderTool.DB_VERSION_P);
		// label = input.getData(label);
		// info.dbVersion = Integer.valueOf(label);
		boolean result = chain.execute();
		return result;
	}

	@Override
	public boolean onFinish(String... args) {
		com.queryme.lib.CommanderTool.setNicknameOnFinishCommander(info);
		return false;
	}

}
