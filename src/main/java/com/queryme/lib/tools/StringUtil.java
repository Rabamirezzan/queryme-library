package com.queryme.lib.tools;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by rramirezb on 09/01/2015.
 */
public class StringUtil {

    public static boolean matches(String input, String regex){
        return Pattern.matches(regex, input);
    }

    public static List<String> getMatchGroups(String input, String regex) {
        Matcher matcher = Pattern.compile(regex).matcher(input);
        List<String> groups = new ArrayList<String>();
        while(matcher.find()){
           groups.add( matcher.group());
        }
        return groups;
    }
}
