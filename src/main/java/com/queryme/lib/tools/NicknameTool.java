package com.queryme.lib.tools;

public class NicknameTool {
	public static final String getNickname(String... args) {
		StringBuilder builder = new StringBuilder();
		int length = args.length;
		for (int i = 0; i < length; i++) {
			String string2 = args[i];
			builder.append(string2);
			if (i < length - 1) {
				builder.append(" ");
			}
		}

		return builder.toString();
	}
}
