package com.queryme.lib.tools;

import java.util.ArrayList;
import java.util.List;

public class ChainSequence{
	private List<Link> links;
	{
		links = new ArrayList<Link>();
	}
	public static abstract class Link{
		protected abstract boolean proccess();
		
	}
	
	public void add(Link link) {
		links.add(link);

	}
	public boolean execute(){
		boolean result = links.isEmpty();
		for (Link link : links) {
			result = link.proccess(); 
			if(!result){
				break;
			}
		}
		return result;
	}
}
