package com.queryme.lib.tools;

import java.util.Date;

public class Chronometer {
	private Date startTime;
	private Date stopTime;
	private long time;

	public void start() {
		startTime = new Date();
	}

	public long stop() {
		stopTime = new Date();
		return time = stopTime.getTime() - startTime.getTime();
	}

	public long getTime() {
		return time;
	}
}
