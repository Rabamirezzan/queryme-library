package com.queryme.lib.tools;

import java.util.SortedMap;
import java.util.TreeMap;

public interface EnumKey {

	public static class Ops {

		/**
		 * The tuples must be EnumKey1, Param1, EnumKey2, Param2, ..., EnumKeyN,
		 * ParamN.
		 * 
		 * @param tuples
		 * @return
		 */
		public static SortedMap<EnumKey, Object> get(Object... tuples) {
			SortedMap<EnumKey, Object> map;
			map = new TreeMap<EnumKey, Object>();
			for (int i = 0; i < tuples.length - 1; i += 2) {
				map.put((EnumKey)tuples[i], tuples[i + 1]);
			}
			return map;
		}

	}
}
