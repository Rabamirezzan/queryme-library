package com.queryme.lib;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Utils {
	public static String getResourceAsString(Class<?> clazz, String resource) {
		InputStream stream = clazz.getResourceAsStream(resource);
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(stream));

		String line = "";
		StringBuilder builder = new StringBuilder();
		while (line != null) {
			try {
				line = reader.readLine();
			} catch (IOException e) {
				line = null;
				e.printStackTrace();
			}
			if (line != null) {
				builder.append(line);
				builder.append("\n");
			}
		}
		return builder.toString();
	}

	public static String[] asStrings(Object... args){
		String[] sArgs = new String[args.length];
		for (int i = 0; i < args.length; i++) {
			sArgs[i] = String.valueOf(args[i]);
		}
		return sArgs;
	}

	public static boolean startsWith(String sentence, String... data) {
		boolean result = false;
		for (String d : data) {
			result |= sentence.startsWith(d);
		}

		return result;
	}
}
