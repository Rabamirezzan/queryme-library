package com.queryme.lib.process;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.queryme.lib.models.Streams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

//import org.apache.commons.codec.binary.Base64;

public class QueryMe {



	// "SELECT productId,code, name, Price.* FROM Product JOIN Price ON  Product.productId = Price.product GROUP BY Product.productId ORDER BY Product.name  limit 20"




	private Streams<?, ?> streams;

	public QueryMe(Streams<?, ?> streams) {
		this.streams = streams;
	}


	public <T>  T execCommand( Type resultClass, String command, String... params){
		T result = null;
		Gson gson = getGson();
		String sparams = gson.toJson(params);
		try {
			streams.connect();
			PrintWriter pw = streams.getOutput();

			pw.println(command);
			pw.flush();

			pw.println(sparams);
			pw.flush();

			// System.out.println("Enviando " + Arrays.asList(params));
			BufferedReader br = streams.getInput();

			String json = br.readLine();

			// System.out.println("obteniendo " + json);
			result = getGson().fromJson(json, resultClass);
			// System.out.println("result " + Arrays.asList(result.columns));

			streams.disconnect();
		} catch (JsonSyntaxException e) {

//			e.printStackTrace();
		} catch (UnknownHostException e) {

//			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		return result;
	}
	@Deprecated
	public static <T>  T execCommand(String host, int port, int timeout,
			Type resultClass, String command, String... params) {
		T result = null;
		Gson gson = getGson();
		String sparams = gson.toJson(params);
		try {
			// System.out.println("Iniciando Socket");
			Socket socket = new Socket();

			socket.connect(new InetSocketAddress(host, port), timeout);
			// System.out.println("Obteniendo printWriter");
			PrintWriter pw = new PrintWriter(socket.getOutputStream());

			pw.println(command);
			pw.flush();

			pw.println(sparams);
			pw.flush();

			// System.out.println("Enviando " + Arrays.asList(params));
			BufferedReader br = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));

			String json = br.readLine();

			// System.out.println("obteniendo " + json);
			result = getGson().fromJson(json, resultClass);
			// System.out.println("result " + Arrays.asList(result.columns));

			pw.close();
			br.close();
		} catch (JsonSyntaxException e) {

//			e.printStackTrace();
		} catch (UnknownHostException e) {

//			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		return result;
	}

	public static  Gson getGson() {
		return new GsonBuilder().serializeNulls().create();
	}



}
