package com.queryme.lib.process;

import com.queryme.lib.Utils;
import com.queryme.lib.tools.StringUtil;

import java.util.List;

/**
 * Created by rramirezb on 12/01/2015.
 */
public class CommandUtilities {
    public static boolean isCommand(String input, String command) {

        return StringUtil.matches(input, command);
    }

    public static boolean isSQLQueryCommand(String params) {
        return Utils.startsWith(params.trim().toLowerCase(), "select", "pragma");
    }

    public static int getNumber(String input) {
        List<String> groups = StringUtil.getMatchGroups(input, "([0-9]+)");
        int result = -1;
        if (!groups.isEmpty()) {
            final int FIRST = 0;
            result = Integer.parseInt(groups.get(FIRST));
            result--;
        }
        return result;
    }
}
