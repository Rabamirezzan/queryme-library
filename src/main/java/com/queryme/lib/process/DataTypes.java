package com.queryme.lib.process;

import com.google.gson.reflect.TypeToken;
import com.queryme.lib.models.*;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by rramirezb on 12/01/2015.
 */
public class DataTypes {

    public static Type getResponseQMDeviceType() {
        Type type = new TypeToken<AppResponse_<QMDevice>>() {
        }.getType();
        return type;
    }

    public static Type getResponseListQMApplicationType() {
        Type type = new TypeToken<AppResponse_<List<QMApplication>>>() {
        }.getType();
        return type;
    }

    public static Type getResponseListQMDatabaseType() {
        Type type = new TypeToken<AppResponse_<List<QMDatabase>>>() {
        }.getType();
        return type;
    }

    public static Type getResponseQMDatabaseType() {
        Type type = new TypeToken<AppResponse_<QMDatabase>>() {
        }.getType();
        return type;
    }

    public static Type getResponseQMResultType() {
        Type type = new TypeToken<AppResponse_<QMResult>>() {
        }.getType();
        return type;
    }
}
