package com.queryme.lib.process;

/**
 * Created by rramirezb on 12/01/2015.
 */
public class QueryMeCommands {
    public static final String EXEC_COMMAND = "[_EXEC_]";
    public static final String QUERY_COMMAND = "[_DATA_QUERY_]";
    public static final String APPLICATIONS_QUERY_COMMAND = "[_APPLICATIONS_LIST_QUERY_]";
    public static final String DATABASES_QUERY_COMMAND = "[_DATABASES_LIST_QUERY_]";
    public static final String SHARED_PREFERENCES_COMMAND = "[_SHARED_PREFERENCES]";
    public static final String DEVICE_INFO_COMMAND = "[_DEVICE_INFO_]";
    public static final String DATABASE_INFO_COMMAND = "[_DATABASE_INFO_]";
    public static final String DATABASE_SELECTION_COMMAND = "[_DATABASE_SELECTION_]";

    public static final String GET_APP_LIST_COMMAND = "(-.*get.* +app.*((list)|(s))).*";
    public static final String SELECT_APP_COMMAND = "(-.*sel.* +app).*([0-9]+).*";
    public static final String SELECT_DATABASE_COMMAND = "(-.*sel.*((data)|(db))).*([0-9]+).*";
    public static final String FORMAT_FIELD_AS_COMMAND = "(-.*format.* +field +.* +as +.*)|(ff +.* as +.*)";
}
