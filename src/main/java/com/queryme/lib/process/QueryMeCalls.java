package com.queryme.lib.process;

import com.queryme.lib.Utils;
import com.queryme.lib.models.*;

import java.util.List;

/**
 * Created by rramirezb on 12/01/2015.
 */
public class QueryMeCalls {

    public int appIndex = 0;
    private QueryMe queryMe;

    public QueryMeCalls(QueryMe queryMe) {
        this.queryMe = queryMe;
    }

    public  QMDevice getDeviceInfo( ) {
        AppResponse<QMDevice> response = queryMe.execCommand(DataTypes.getResponseQMDeviceType(),
                QueryMeCommands.DEVICE_INFO_COMMAND, "");
        return response.data();
    }

    public  QMDevice setDatabaseInfo( String appPackage, String dbName, int dbVersion) {
        AppResponse<QMDevice> response = queryMe.execCommand(DataTypes.getResponseQMDeviceType(),
                QueryMeCommands.DATABASE_INFO_COMMAND, dbName, String.valueOf(dbVersion), appPackage);
        return response.data();
    }

    public AppResponse_<List<QMApplication>> getApplicationsList(){
        AppResponse_<List<QMApplication>> response = queryMe.execCommand(
                DataTypes.getResponseListQMApplicationType(),
                QueryMeCommands.APPLICATIONS_QUERY_COMMAND);
        return response;
    }

    public AppResponse_<List<QMDatabase>> getDatabases(int appIndex){
        AppResponse_<List<QMDatabase>> response = queryMe.execCommand(
                DataTypes.getResponseListQMDatabaseType(),
                QueryMeCommands.DATABASES_QUERY_COMMAND, String.valueOf(appIndex));
        return response;
    }

    public AppResponse_<QMDatabase> selectDatabase(int appIndex, int dbIndex){
        AppResponse_<QMDatabase> response = queryMe.execCommand(
                DataTypes.getResponseQMDatabaseType(),
                QueryMeCommands.DATABASE_SELECTION_COMMAND, Utils.asStrings(appIndex, dbIndex));
        return response;
    }

    public AppResponse_<QMResult> executeQuery(String... params){
        AppResponse_<QMResult> response = queryMe.execCommand(
                DataTypes.getResponseQMResultType(), QueryMeCommands.QUERY_COMMAND, params);
        return response;
    }
    public AppResponse_<QMResult> execute(String... params){
        AppResponse_<QMResult> response = queryMe.execCommand(
                DataTypes.getResponseQMResultType(), QueryMeCommands.EXEC_COMMAND, params);
        return response;
    }


}
