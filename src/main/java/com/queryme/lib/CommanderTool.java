package com.queryme.lib;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.queryme.lib.datainput.UserInput;
import com.queryme.lib.models.Info;
import org.apache.commons.validator.routines.InetAddressValidator;

import java.lang.reflect.Type;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommanderTool {

	public static interface ICommander {

		public String[] getRequiredParams();

		public boolean onNickname(String... args);

		public boolean onIP(String... args);

		public boolean onPort(String... args);

		public boolean onApp(String... args);

		public boolean onDBName(String... args);

		public boolean onDBVersion(String... args);

		public boolean onHelp(String... args);

		public boolean onWizard(String... args);

		public boolean onFinish(String... args);

	}

	public static final String NICKNAME_P = "nickname";
	public static final String IP_P = "ip";
	public static final String PORT_P = "port";
	public static final String DB_NAME_P = "dbname";
	public static final String DB_VERSION_P = "dbversion";
	public static final String APP_P = "app";
	public static final String HELP_P = "help";
	public static final String WIZARD_P = "wizard";
	public static final String VERSION_P = "version";
	public static final String ARRAYLIST_COLUMNS_P = "arraylistcolumns";
	public static final String DATE_COLUMNS_P = "datecolumns";
	public static final String BOOLEAN_COLUMNS_P = "booleancolumns";
	private static final String COMMANDS_ARGS_PATTERN = "-[a-zA-Z]+( +[[^-].]*)*";

	// public static boolean hasParameters(String param) {
	// boolean result = !(param.equals("-".concat(HELP_P)) || param.equals("-"
	// .concat(WIZARD_P)));
	// return result;
	// }

	public static String joinArgs(String[] args) {
		StringBuilder result = new StringBuilder();
		int lenght = args.length;
		for (int i = 0; i < lenght; i++) {
			result.append(args[i]);
			if (i < lenght - 1) {
				result.append(" ");
			}
		}
		return result.toString();
	}

	public static Map<String, List<String>> getParams(String[] args) {
		Pattern argsPattern = Pattern.compile(COMMANDS_ARGS_PATTERN,
				Pattern.CASE_INSENSITIVE);

		String paramArgs = joinArgs(args);
		Matcher m = argsPattern.matcher(paramArgs);

		Map<String, List<String>> map;
		map = new TreeMap<String, List<String>>();
		while (m.find()) {
			String g = m.group().trim();
			String[] params = g.split(" ");
			List<String> ps = new ArrayList<String>(Arrays.asList(params));
			if (params.length > 0) {
				ps.remove(0);
				map.put(params[0], ps);
			}

		}
		// System.out.println(map);
		return map;
	}
	
	public static final String getCommand(String name){
		String data = name;
		if(name!=null){
			data = "-".concat(name);
		}
		return data; 
	}

	public static boolean equalsTo(String input, String command) {
		return input.equals("-".concat(command));
	}

	public static String getCommanderVersion(
			Map<String, List<String>> parameters, String defaultVersion) {

		String version = "";
		if (parameters.containsKey(VERSION_P)) {
			List<String> values = parameters.get("-".concat(VERSION_P));
			if (values != null && !values.isEmpty()) {
				version = values.get(0);
			}

		}
		if (version.equals("")) {
			version = defaultVersion;
		}
		return version;
	}

	public static boolean executeCommands(Map<String, List<String>> parameters,
			ICommander commander) {
		Iterator<String> params = parameters.keySet().iterator();
		List<String> reqParams = new ArrayList<String>(Arrays.asList(commander
				.getRequiredParams()));
		boolean result = true;
		while (params.hasNext()) {
			String param = params.next();
			String[] args = parameters.get(param).toArray(new String[] {});
			if (equalsTo(param, HELP_P)) {
				result = commander.onHelp(args);
			} else if (equalsTo(param, WIZARD_P)) {
				result = commander.onWizard(args);
			} else if (equalsTo(param, IP_P)) {
				if (commander.onIP(args)) {
					reqParams.remove(IP_P);
				}

			} else if (equalsTo(param, PORT_P)) {
				if (commander.onPort(args)) {
					reqParams.remove(PORT_P);
				}
			} else if (equalsTo(param, NICKNAME_P)) {
				if (commander.onNickname(args)) {
					reqParams.remove(NICKNAME_P);
				}
			} else if (equalsTo(param, APP_P)) {
				if (commander.onApp(args)) {
					reqParams.remove(APP_P);
				}
			} else if (equalsTo(param, DB_NAME_P)) {
				if (commander.onDBName(args)) {
					reqParams.remove(DB_NAME_P);
				}
			} else if (equalsTo(param, DB_VERSION_P)) {
				if (commander.onDBVersion(args)) {
					reqParams.remove(DB_VERSION_P);
				}
			}
		}

//		if (parameters.isEmpty() || reqParams.size() > 0
//				&& reqParams.size() != commander.getRequiredParams().length) {
//			System.out.println(String.format("And %s? Complete the param(s)",
//					reqParams));
			UserInput input = new UserInput();
			String response = input.getData("Run wizard? Y/N");
			boolean runWizard = response.toLowerCase().startsWith("y");
			result = runWizard;
			if (runWizard) {
				result = commander.onWizard();
			}
//		} else {
//			commander.onFinish();
//		}

		return result;
	}

	public static void showHelp() {
		String resource = Utils.getResourceAsString(CommanderTool.class,
				"/help.txt");
		System.out.println(resource);
	}

	public static Map<String, String> getWizard() {
		String resource = Utils.getResourceAsString(CommanderTool.class,
				"/wizard.txt");
		Gson gson = new Gson();
		Type type = new TypeToken<Map<String, String>>() {
		}.getType();
		Map<String, String> wizard = gson.fromJson(resource, type);
		return wizard;
	}

	public static void setNicknameOnFinishCommander(Info info) {
		if (info.nickname == null || info.nickname.trim().equals("")) {
			info.nickname = info.ip.concat(":").concat(
					String.valueOf(info.port));

		}
	}

	public static boolean isValidIP(String ip) {
		return InetAddressValidator.getInstance().isValid(ip);
	}
}
