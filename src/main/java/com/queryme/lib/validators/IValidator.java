package com.queryme.lib.validators;


import com.queryme.lib.tools.ProcessID;

public interface IValidator<T> {
	public boolean validate(ProcessID process, T object);

	public static final class Ops {
		public static final <T> boolean validate(ProcessID process, T object,
				IValidator<T>... validators) {
			boolean result = true;
			for (IValidator<T> validator : validators) {
				result = validator.validate(process, object);

				if (!result) {
					break;
				}
			}
			return result;
		}

		
	}
}
