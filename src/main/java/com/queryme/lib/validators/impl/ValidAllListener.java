package com.queryme.lib.validators.impl;



public class ValidAllListener implements com.queryme.lib.validators.IValidationListener<String> {

	private static Class<?>[] classes; 

	static{
		classes = new Class<?>[] { com.queryme.lib.validators.impl.ValidIP.class,
				com.queryme.lib.validators.impl.ValidPort.class, com.queryme.lib.validators.impl.ValidAppInformation.class };
	}
	@Override
	public void actionPerformed(Event<String> event) {
		if (!event.isValid()) {
			String name = event.getParameter(com.queryme.lib.validators.impl.GlobalParams.PARAM_NAME);
			String line = String.format("The %s '%s' is not valid.",
					name, event.getSource());
			System.out.println(line);
		}
	}

	@Override
	public <V extends com.queryme.lib.validators.IValidator<?>> Class<V>[] getValidationClasses() {

		return (Class<V>[]) classes;
	}

}
