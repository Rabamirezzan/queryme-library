package com.queryme.lib.validators.impl;


import com.queryme.lib.tools.ProcessID;
import com.queryme.lib.validators.IValidationListener;
import com.queryme.lib.validators.IValidator;

public class ValidAppInformation implements IValidator<String> {

	@Override
	public boolean validate(ProcessID process,String object) {
		boolean result = !(object==null||object.trim().equals(""));
		IValidationListener.Ops.fireEventWithParams(process, object, result, this.getClass(),
				GlobalParams.PARAM_NAME, "App info");
		
		return result;
	}
	
}
