package com.queryme.lib.validators.impl;


import com.queryme.lib.tools.ProcessID;
import com.queryme.lib.validators.IValidationListener;
import com.queryme.lib.validators.IValidator;

public class ValidPort implements IValidator<String> {

	@Override
	public boolean validate(ProcessID process, String object) {

		boolean result = true;
		try {
			Integer.valueOf(object);
		} catch (NumberFormatException e) {
			result = false;

		}
		IValidationListener.Ops.fireEventWithParams(process, object, result,
				this.getClass(), GlobalParams.PARAM_NAME, "Port");
		return result;
	}

}
