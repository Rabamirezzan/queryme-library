package com.queryme.lib.validators.impl;


import com.queryme.lib.CommanderTool;
import com.queryme.lib.tools.ProcessID;
import com.queryme.lib.validators.IValidationListener;
import com.queryme.lib.validators.IValidator;

public class ValidIP implements IValidator<String> {

	@Override
	public boolean validate(ProcessID process, String ip) {
		boolean result = CommanderTool.isValidIP(ip);

		IValidationListener.Ops.fireEventWithParams(process, ip, result,
				this.getClass(), GlobalParams.PARAM_NAME, "IP");
		return result;
	}

}
