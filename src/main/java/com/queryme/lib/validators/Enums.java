package com.queryme.lib.validators;

import com.queryme.lib.tools.EnumKey;

import java.util.SortedMap;
import java.util.TreeMap;



public class Enums {

	public static enum Keys implements EnumKey {
		hola, mundo;
	}
	
	public static void main(String[] args) {
		SortedMap<EnumKey, Object> map;
		map = new TreeMap<EnumKey, Object>();
		
		map.put(Keys.hola, 20);
	}
}
