package com.queryme.lib.validators;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.queryme.lib.tools.EnumKey;
import com.queryme.lib.tools.ProcessID;

import java.util.Collection;
import java.util.SortedMap;
import java.util.TreeMap;

public interface IValidationListener<T> {
	public <V extends com.queryme.lib.validators.IValidator<?>> Class<V>[] getValidationClasses();

	public void actionPerformed(Event<T> event);

	public static final class Ops {

		private static Multimap<Class<? extends com.queryme.lib.validators.IValidator>, IValidationListener> listeners;

		public static Multimap<Class<? extends com.queryme.lib.validators.IValidator>, IValidationListener> getListeners() {
			if (listeners == null) {
				listeners = ArrayListMultimap.create();
			}
			return listeners;
		}

		@SuppressWarnings({ "unchecked", "rawtypes" })
		public static final <L extends IValidationListener> void register(
				Class<L> listenerClass) {
			try {
				L listener = listenerClass.newInstance();
				Class[] classes = listener.getValidationClasses();
				for (Class class1 : classes) {
					getListeners().put(class1, listener);
				}

			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@SuppressWarnings("unchecked")
		private static Class<com.queryme.lib.validators.IValidator> getC() {

			StackTraceElement[] trace = Thread.currentThread().getStackTrace();
			String className = trace[2].getClassName();
			Class<?> clazz = null;
			try {
				clazz = Class.forName(className);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return (Class<com.queryme.lib.validators.IValidator>) clazz;
		}

		@SuppressWarnings({ "rawtypes", "unchecked" })
		public static final <T, V extends com.queryme.lib.validators.IValidator<T>> void fireEvent(
				ProcessID process, T object, boolean result,
				Class<V> validatorClass) {
			fireEvent(process, object, result, validatorClass,
					new TreeMap<EnumKey, Object>());
		}

		@SuppressWarnings({ "rawtypes", "unchecked" })
		public static final <T, V extends com.queryme.lib.validators.IValidator<T>> void fireEvent(
				ProcessID process, T object, boolean result,
				Class<V> validatorClass, SortedMap<EnumKey, Object> parameters) {

			// Class<V> validatorClass = (Class<V>)getC();

			Collection<IValidationListener> ls = getListeners().get(
					validatorClass);
			Event<T> event = new Event<T>(process, object, result, parameters);
			for (IValidationListener iValidationListener : ls) {
				iValidationListener.actionPerformed(event);
			}

		}

		@SuppressWarnings({ "rawtypes", "unchecked" })
		public static final <T, V extends com.queryme.lib.validators.IValidator<T>> void fireEventWithParams(
				ProcessID process, T object, boolean result,
				Class<V> validatorClass, Object... tuples) {

			SortedMap<EnumKey, Object> parameters = EnumKey.Ops.get(tuples);
			// Class<V> validatorClass = (Class<V>)getC();

			Collection<IValidationListener> ls = getListeners().get(
					validatorClass);
			Event<T> event = new Event<T>(process, object, result, parameters);
			for (IValidationListener iValidationListener : ls) {
				iValidationListener.actionPerformed(event);
			}

		}
	}

	public static class Event<T> {

		private ProcessID process;
		private SortedMap<EnumKey, Object> parameters;

		private T source;

		private boolean valid;

		public Event(ProcessID process, T source, boolean valid) {
			this(process, source, valid, new TreeMap<EnumKey, Object>());

		}

		public Event(ProcessID process, T source, boolean valid,
				SortedMap<EnumKey, Object> parameters) {
			super();
			this.process = process;
			this.source = source;
			this.valid = valid;
			this.initMap(parameters);
		}

		private void initMap(SortedMap<EnumKey, Object> map) {
			this.parameters = map;
		}

		public ProcessID getProcess() {
			return process;
		}

		public T getSource() {
			return source;
		}

		public boolean isValid() {
			return valid;
		}

		@SuppressWarnings("unchecked")
		public <P> P getParameter(EnumKey key) {
			return (P) parameters.get(key);
		}
	}
}
