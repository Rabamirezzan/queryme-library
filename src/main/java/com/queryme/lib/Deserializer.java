package com.queryme.lib;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import org.apache.commons.codec.binary.Base64;

public class Deserializer {
	public static<T> T toObject(String base64Data) {
//		ByteArrayOutputStream arrayOut = new ByteArrayOutputStream();
//		
//		ObjectOutputStream out = new ObjectOutputStream(arrayOut);
//		
//		List<Integer> list = new ArrayList<Integer>();
//		list.add(1);
//		list.add(2);
//		list.add(3);
//		out.writeObject(list);
//		
//		String sObject = Base64.encodeBase64String(arrayOut.toByteArray());
		
//		System.out.println("OUT: "+sObject);
//		System.out.println("OUT: "+arrayOut);
		Object instance = null;
		try {
			ByteArrayInputStream arrayIn = new ByteArrayInputStream(Base64.decodeBase64(base64Data));//sObject.getBytes());
			ObjectInputStream in = new ObjectInputStream(arrayIn);
			instance = (T) in.readObject();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return (T)instance;
		
	}
}
