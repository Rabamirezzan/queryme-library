package com.queryme.lib;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Date;

public class DateMain {
	static BufferedReader br = new BufferedReader(new InputStreamReader(
			System.in));

	public static void main(String[] args) {

		boolean end= false;
		do{
		try {

				System.out.println("Set the time (millis)");
			String time = br.readLine();
			long ltime = Long.parseLong(time);
			Date yourTime = new Date(ltime);
			Date today = new Date();
			System.out.println("yourTime = " + yourTime + ": "
					+ yourTime.getTime());
			System.out.println("Today = " + today + " " + today.getTime());

			String l = br.readLine();
			if (l.contains("c")) {
				end = true;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}while(!end);
	}
}
