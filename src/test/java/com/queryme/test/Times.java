package com.queryme.test;

import org.junit.Test;

import java.util.Date;

/**
 * Created by rramirezb on 19/02/2015.
 */
public class Times {

    @Test
    public void test(){
//        columns: [orderId, created, syncDate]
//        0001 {
//            orderId:1,
//                    created:1424386513024,
//                    syncDate:1424386534206
//        }
//        0002 {
//            orderId:2,
//                    created:1424386719194,
//                    syncDate:1424388021667
//        }

        Long[] times = new Long[]{
                1424386513024L,1424386534206L,1424386719194L,1424388021667L
                ,1426882931456L,1426883013605L
        };

        for (int i = 0; i < times.length; ) {
            Long created = times[i++];
            Long sync = times[i++];
            System.out.printf("created %s sync %s\n", new Date(created), new Date(sync));
        }


    }
}
