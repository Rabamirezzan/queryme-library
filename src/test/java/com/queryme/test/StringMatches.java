package com.queryme.test;

import com.queryme.lib.process.QueryMeCommands;
import com.queryme.lib.tools.StringUtil;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * Created by rramirezb on 09/01/2015.
 */
public class StringMatches {
    @Test
    public void testGET_APP_LIST_COMMAND(){
        boolean result = StringUtil.matches("-get the applications f'in list ok", QueryMeCommands.GET_APP_LIST_COMMAND);
        Assert.assertTrue("Si matchea extended", result);

        result = StringUtil.matches("-  getapplist va?", QueryMeCommands.GET_APP_LIST_COMMAND);
        Assert.assertTrue("Si matchea reduced", result);

        result = StringUtil.matches("- mmm getapps the nuexz", QueryMeCommands.GET_APP_LIST_COMMAND);
        Assert.assertTrue("Si matchea app.*s", result);
    }

    @Test
    public void testSELECT_APP_COMMAND(){
        boolean result = StringUtil.matches("-get the applications f'in list ok", QueryMeCommands.SELECT_APP_COMMAND);
        Assert.assertFalse("No matchea extended", result);

//        result = StringUtil.matches("-select application 123", QueryMe.SELECT_APP_COMMAND);
//        Assert.assertTrue("Si matchea reduced", result);

        result = StringUtil.matches("1234 45345", "([0-9]+).*");
        Assert.assertTrue("ok", result);
        result = StringUtil.matches("-select application 123 ok?", QueryMeCommands.SELECT_APP_COMMAND);
        Assert.assertTrue("matchea app.*s", result);
    }

    @Test
      public void testGetNumber(){
        String data= "select app 234 sdf";
        List<String> result = StringUtil.getMatchGroups(data, "([0-9]+)");
        Assert.assertEquals("Results ", 1, result.size());
        Assert.assertEquals("Same number", "234", result.get(0));
    }

    @Test
    public void formatFieldAsTest(){
        String data= "-format field X as      Y";
        boolean result = StringUtil.matches(data, QueryMeCommands.FORMAT_FIELD_AS_COMMAND);
        Assert.assertTrue("match format field as command "+QueryMeCommands.FORMAT_FIELD_AS_COMMAND, result);
        testRegex(data, "(as[\\s]{0,100})", "([\\S].*)");
    }

    private void testRegex(String data, String no_capturing, String capturing){
//        (?<name>X)	X, as a named-capturing group
//        (?:X)	X, as a non-capturing group
//        (?idmsuxU-idmsuxU) 	Nothing, but turns match flags i d m s u x U on - off
//        (?idmsux-idmsux:X)  	X, as a non-capturing group with the given flags i d m s u x on - off
//        (?=X)	X, via zero-width positive lookahead
//        (?!X)	X, via zero-width negative lookahead
//        (?<=X)	X, via zero-width positive lookbehind
//        (?<!X)	X, via zero-width negative lookbehind
//        (?>X)	X, as an independent, non-capturing group
        String[] list = new String[]{
                "(?:%s)", "(?=%s)","(?!%s)","(?<=%s)","(?<!%s)","(?>%s)"
        };

        for (int i = 0; i < list.length; i++) {
            String s = list[i];
            String r = String.format(s+"%s", no_capturing, capturing);
            try {
                System.out.printf("of %s are: ", r);
                List<String> groups = StringUtil.getMatchGroups(data, r);
                System.out.printf("%s\n", groups);
            } catch (Exception e) {
                System.out.printf(".. error%s\n",e.getMessage());

            }
        }


    }

    @Test
    public void otherTest(){
       String x = "(?:http|ftp)://([^/\r\n]+)(/[^\r\n]*)?";

        List<String> groups = StringUtil.getMatchGroups("http://stackoverflow.com/", x);

        System.out.printf("Groups %s\n", groups);

    }
}
