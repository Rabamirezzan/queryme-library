package com.queryme.test;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by rramirezb on 23/01/2015.
 */
public class TimeBetweenDates {

    @Test
    public void test() {

        String[] dates = new String[]{"2015.01.23 10.40.41.168",
                "2015.01.23 11.00.02.751",
                "2015.01.23 11.20.37.336",
                "2015.01.23 11.20.37.336",
                "2015.01.23 11.30.01.991",
                "2015.01.23 12.00.18.951",
                "2015.01.23 12.20.44.836",
                "2015.01.23 12.20.44.836",
                "2015.01.23 15.30.01.012",
                "2015.01.23 15.30.01.012",
                "2015.01.23 15.41.55.341",
                "2015.01.23 15.41.55.341",
                "2015.01.23 15.50.21.239",
                "2015.01.23 15.50.21.239",
                "2015.01.23 15.50.21.239"};
        SimpleDateFormat sdf = new SimpleDateFormat("yyy.MM.dd HH.mm.ss.SSS");


        String ant = null;
        for (int i = 0; i < dates.length; i++) {
            String date = dates[i];
            try {

                if (ant != null) {
                    Date d1 = sdf.parse(ant);
                    Date d2 = sdf.parse(date);
                    System.out.printf("from %s to %s: %4.2f minutes\n", ant, date, ((d2.getTime() - d1.getTime()) / 1000.0 / 60.0));
                }
                ant = date;

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }
}
